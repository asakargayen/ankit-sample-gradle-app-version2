import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException
env.ms_teams_url = "https://outlook.office.com/webhook/df89cacf-a047-4e9a-8dd1-db6a572307c0@36da45f1-dd2c-4d1f-af13-5abe46b99921/JenkinsCI/51facbdb3b894fcdac9b50ec53d68f85/814d8929-b5b6-4483-93af-2b5d8562b035"
def gradlew(String... args) {
    sh "./gradlew ${args.join(' ')} -s"
}
pipeline
{
  agent any
  triggers {
        pollSCM('* * * * *')
  }
  options {
       office365ConnectorWebhooks([[
                   startNotification: true,
                       url: 'https://outlook.office.com/webhook/df89cacf-a047-4e9a-8dd1-db6a572307c0@36da45f1-dd2c-4d1f-af13-5abe46b99921/JenkinsCI/51facbdb3b894fcdac9b50ec53d68f85/814d8929-b5b6-4483-93af-2b5d8562b035'
           ]]
        )
    }
  environment {
        AWS_CREDENTIALS = """${sh(returnStdout: true, script: 'aws sts assume-role --role-arn arn:aws:iam::054273887971:role/opendata_jenkins_admin --role-session-name "jenkins_connection"').trim()}"""
        REGISTRY_ENDPOINT = "054273887971.dkr.ecr.us-east-1.amazonaws.com/opendata-demo"
        registryCredential = 'ecr:us-east-1:ecr'
        IMAGE    = "ankit-hello-gradle-version1"
        IMAGE_TAG = "ankit-hello-gradle-version1-${env.BUILD_NUMBER}"
        ECR_LOGIN = """${sh(returnStdout: true, script: 'aws ecr get-login-password').trim()}"""
    }
  stages
    {
        stage('Set assume role credentials')
        {
            steps
            {
		       script{
                    env.AWS_ACCESS_KEY_ID="""${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.AccessKeyId\'|tr -d \'"\'').trim()}"""
                    env.AWS_SECRET_ACCESS_KEY="""${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.SecretAccessKey\'|tr -d \'"\'').trim()}"""
                    env.AWS_SESSION_TOKEN="""${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.SessionToken\'|tr -d \'"\'').trim()}"""
                    env.AWS_DEFAULT_REGION="us-east-1"
              }
            }
        }
        stage('gradle Compile') {
            steps {
             sh '''chmod +x ./gradlew
             	    ./gradlew clean'''

            }
        }
        stage('gradle Build'){
          steps {
              // gradlew('build')
            sh '''chmod +x ./gradlew
            	    ./gradlew clean'''
          }
        }
         stage('JUnit Test') {
          steps {
            gradlew('clean', 'test')
          }
        }
          stage('Publish Junit Results'){
          steps {
            retry(3) {
              junit 'build/test-results/test/*.xml'
            }
          }
        }
        stage('Code Quality') {
          steps {
            script {
              try{
                checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
              }catch(e){
                echo e
              }
            }
          }
        }
        stage('SonarQube analysis') {
          steps {
              withSonarQubeEnv('sonarqube') { // Will pick the global server connection you have configured
                 gradlew('clean', 'sonarqube')
                //  sh './gradlew sonarqube'
                // sh './gradlew org.sonarsource.scanner.gradle: sonarqube-gradle-plugin:2.5:sonarqube'
              }
            }
        }
        stage('Build Docker Image') {
          steps {
            echo 'Build Images'
            script {
              docker.withRegistry("${REGISTRY_ENDPOINT}") {
                    withCredentials([string(credentialsId: 'ecr', variable: 'SECRET')]) { //set SECRET with the credential content
                          sh '''
                            set +x aws ecr get-login-password  
                            docker build -t ${IMAGE} .
                          '''
                        }
                    }
                  }
              }
        }
          stage('Tag Docker Image') {
          steps {
            script {
              docker.withRegistry("${REGISTRY_ENDPOINT}") {
                   withCredentials([string(credentialsId: 'ecr', variable: 'SECRET')]) { //set SECRET with the credential content
                    sh '''
                      docker tag ${IMAGE}:latest ${REGISTRY_ENDPOINT}:${IMAGE_TAG}
                    '''
              }
              }
            }
        }
        }
        stage('Waiting for Approval')
        {
            steps
            {
                office365ConnectorSend webhookUrl: "$ms_teams_url",
                    message: "Waiting for Approval ${env.JOB_NAME} Build #${env.BUILD_NUMBER} commited by @${user} [View on Jenkins](${env.JOB_URL})<br>Pipeline duration: ${currentBuild.durationString}",
                    status: 'Waiting for Approval!',
                    factDefinitions: [[name: "Test By", template: user]]
                input "Approve to Apply?"
            }
        }
        stage('Publish Docker Image to ECR') {
          steps {
            echo 'Push Images'
            script {
              docker.withRegistry("${REGISTRY_ENDPOINT}") {
               withCredentials([string(credentialsId: 'ecr', variable: 'SECRET')]) { //set SECRET with the credential content
                sh '''
                 docker login -u AWS -p ${ECR_LOGIN} https://${REGISTRY_ENDPOINT}
                 docker push ${REGISTRY_ENDPOINT}:${IMAGE_TAG}
                '''
              }
              }
            }
          }
        }
        // stage('Secrets Manager tf Plan')
        // {
        //     steps
        //     {
        //        withCredentials([string(credentialsId: 'app1_secret', variable: 'SECRET')]) {

        //         sh '''
        //          cd secrets-manager-sampleappv2
        //          terragrunt plan -input=false
        //         '''
        //        }
        //         office365ConnectorSend webhookUrl: "$ms_teams_url",
        //             message: "Plan ${env.JOB_NAME} Build #${env.BUILD_NUMBER} commited by @${user} [View on Jenkins](${env.JOB_URL})<br>Pipeline duration: ${currentBuild.durationString}",
        //             status: 'See the Plan!',
        //             factDefinitions: [[name: "Test By", template: user]]
        //     }
        // }
        //  stage('Waiting for Approval')
        // {
        //     steps
        //     {
        //         office365ConnectorSend webhookUrl: "$ms_teams_url",
        //             message: "Waiting for Approval ${env.JOB_NAME} Build #${env.BUILD_NUMBER} commited by @${user} [View on Jenkins](${env.JOB_URL})<br>Pipeline duration: ${currentBuild.durationString}",
        //             status: 'Waiting for Approval!',
        //             factDefinitions: [[name: "Test By", template: user]]
        //         input "Approve to Apply?"
        //     }
        // }
        // stage('Secrets Manager tf Apply')
        // {
        //     steps
        //     {
        //      sh '''
        //         cd secrets-manager-sampleappv2
        //         terragrunt apply -no-color --terragrunt-non-interactive -input=false -auto-approve

        //     '''
        //         office365ConnectorSend webhookUrl: "$ms_teams_url",
        //             message: "Success ${env.JOB_NAME} Build #${env.BUILD_NUMBER} commited by @${user} [View on Jenkins](${env.JOB_URL})<br>Pipeline duration: ${currentBuild.durationString}",
        //             status: 'Success!',
        //             factDefinitions: [[name: "Test By", template: user]]
        //     script {
        //               APP_SECRET_ARN = """${sh(returnStdout: true, script: 'aws secretsmanager get-secret-value --secret-id sample-app-v2-secret --query ARN --output text --region us-east-1').trim()}"""
        //     }
        //     }
        //     // script {
        //     //   export app-secret-arn = sh 'aws secretsmanager get-secret-value --secret-id sample-app-secret --query ARN --output text --region us-east-1'
        //     //   //  app-secret-arn = """${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.AccessKeyId\'|tr -d \'"\'').trim()}"""
        //     // }

        // }
      stage('Gradle App Helm S3 init')
        {
            steps
            {
		        sh '''
                    helm s3 init s3://opendata-backend-state/charts/ankit-sample-app
                    helm repo add ankit s3://opendata-backend-state/charts/ankit-sample-app
                '''
            }
        }
        stage('Gradle App Helm package')
        {
            steps
            {
		        sh '''
                    helm package charts/ankit
                '''
            }
        }
        stage('Gradle App Helm deploy chart to S3')
        {
            steps
            {
		        sh '''
                  helm s3 push gradle-helm-chart-v2-ankit-0.1.0.tgz ankit --force
                '''
            }
        }
    }
}